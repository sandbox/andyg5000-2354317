<?php

/**
 * @file
 * Provides default rule for commerce_status_alert.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_status_alert_default_rules_configuration() {
  $rule = '{ "commerce_status_alert_orders_with_incorrect_status" : {
      "LABEL" : "Load paid orders with incorrect status.",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_status_alert", "rules" ],
      "ON" : { "cron" : [] },
      "DO" : [
        { "commerce_status_alert_load_orders" : { "PROVIDE" : { "orders" : { "orders" : "Commerce Orders" } } } },
        { "LOOP" : {
            "USING" : { "list" : [ "orders" ] },
            "ITEM" : { "current_order" : "Current order" },
            "DO" : [
              { "mail" : {
                  "to" : "[site:mail]",
                  "subject" : "Order with incorrect status.",
                  "message" : "The following order is paid, but has an incorrect order status.\r\n\r\n[current-order:admin-url]\r\n",
                  "language" : [ "" ]
                }
              }
            ]
          }
        }
      ]
    }
  }';

  $configs['commerce_status_alert_orders_with_incorrect_status'] = rules_import($rule);

  return $configs;
}
