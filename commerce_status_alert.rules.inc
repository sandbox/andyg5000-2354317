<?php

/**
 * @file
 * Provides rules hooks for commerce_status_alert
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_status_alert_rules_action_info() {
  return array(
    'commerce_status_alert_load_orders' => array(
      'label' => t('Load paid order with an incorrect status'),
      'group' => t('Commerce Order'),
      'provides' => array(
        'orders' => array(
          'type' => 'list<commerce_order>',
          'label' => t('Commerce Orders'),
        ),
      ),
    ),
  );
}

/**
 * Action callback for loading paid orders with incorrect status.
 */
function commerce_status_alert_load_orders() {
  $orders = array();

  $cart = commerce_order_statuses(array('state' => 'cart'));
  $checkout = commerce_order_statuses(array('state' => 'checkout'));
  $statuses = array_keys(array_merge($cart, $checkout));

  $query = db_select('commerce_payment_transaction', 'cpt');
  $query->join('commerce_order', 'o', 'cpt.order_id = o.order_id');
  $query
    ->fields('cpt', array('order_id'))
    ->condition('cpt.status', 'failure', '!=')
    ->condition('cpt.amount', 0, '>')
    ->condition('o.status', $statuses, 'IN');
  $order_ids = $query->execute()->fetchAll();

  if (!empty($order_ids)) {
    foreach($order_ids as $order_id) {
      $orders[] = commerce_order_load($order_id->order_id);
    }
  }

  return array('orders' => $orders);
}
